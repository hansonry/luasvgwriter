# LuaSVGWriter

A Lua library that can be used to produce SVG Files and Strings.

## Examples

You can find examples in the `examples` directory. They will be maintained
with the source code, so they will be a source of truth for the API


```
--- redBox.lua

-- Get a handle to the library
local SVGW = require "SVGWriter"

-- Create a new SVG document
--                           width
--                           |   height
local doc = SVGW.Document:new(30, 30)

-- Creates a rectangle and adds it to the documentation. Also rotates
-- the rectangle 45 degrees on center
--          x
--          |   y                    stroke                    angle(degrees)
--          |   |   width            |        stroke-width     |   around x
--          |   |   |   height       |        |  fill          |   |   around y
doc:addRect(10, 10, 10, 10):setStyle("black", 2, "red"):rotate(45, 15, 15)

-- Write document to file
doc:writeToFile("examples/output/redBox.svg")

```

[redBox.lua](examples/redBox.lua)
![redBox.lua Image](examples/output/redBox.png)

[hsl.lua](examples/hsl.lua)
![hsl.lua Image](examples/output/hsl.png)

[use.lua](examples/use.lua)
![use.lua Image](examples/output/use.png)

[gradient.lua](examples/gradient.lua)
![gradient.lua Image](examples/output/gradient.png)

[marker.lua](examples/marker.lua)
![marker.lua Image](examples/output/marker.png)

[hueKey.lua](examples/hueKey.lua)
![hueKey.lua Image](examples/output/hueKey.png)

[shapes.lua](examples/shapes.lua)
![shapes.lua Image](examples/output/shapes.png)

[pattern.lua](examples/pattern.lua)
![pattern.lua Image](examples/output/pattern.png)

## Project Goals

The following are features of this project:

* Programaticaly create SVG documents or strings that can be read
  by a majority of popular readers/parsers
* Provides feedback if you are attempting to build an improper
  SVG document
* One to one document creation. The same input will produce the same output.
* Objects should be mutable. Once an object has been created feel free
  to change it's parameters. Output should be correctly created unless extreem
  changes have been made.
* SVG tags that include a "-" like `stroke-width` can be defined with a dash
  or an underscore Ex: `stroke_width`.

The following are things this project will **NOT** support:

* Reading/Parsing SVG Files
* Abstract away SVG file format knowlage. 
    * You will need to know how SVG documents work in order to use this tool.
* Speed. 
    * The focus of the code will be on readablity and maintainability. 
      That being said, at the time of this writing, I have not seen any example
      or test that didn't complete immediately.
* Strict procedural or object orented API. 
    * You will get a little of everything.

The following are developer goals:

* Unit Tests shall be kept up to date at all times
* Examples shall be kept up to date at all times
* Code shall be simple to read and easy to maintain
* The API is compact, flexable, and easy to read given knowlage of SVG.
* Optimizations will be made with readablity in mind and with concrete 
  evedence of imporvment.

## Progress

Basic functionlaity completed.

See [TODO.md](TODO.md) for more information.

## API

This library is still in development and the API is subject to change.
See the tests and examples to get an idea of how the library works. They
will be maintined with the library so they will be up to date.

## Contributing

Pull or Merge Requests are welcome! I will work with you to get your changes
into the code base. I am a busy person and easily distracted, keep that in
mind. I expect respect of myself and others.

I created this project as a way to scrach and itch. I only a basic idea of 
how SVGs worked when I started this project. 

That being said. I will make the final call on whether or not your 
contributions make it into the project. I do this for fun. If your code
doesn't increase the fun factor for me, I will not accept it. You are welcome
to fork the project if you like. I would take it as a complement 
if you forked.

## License

GPLv3 [https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)



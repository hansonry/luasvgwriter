local SVGW = require "SVGWriter"

function stringCheck(a, b)
   function makeIntoLinesArray(text)
      local lines = {}
      for line in string.gmatch(text, "[^\n]+") do
         table.insert(lines, line)
      end
      return lines
   end
   function createPrintableLine(line)
      if line then
         return string.format('"%s"', line)
      end
      return "(nil)"
   end
   if a == b then
      return true
   end
   local aLines = makeIntoLinesArray(a)
   local bLines = makeIntoLinesArray(b)

   local max
   if #aLines > #bLines then
      max = #aLines
   else
      max = #bLines
   end

   local flagLine
   for i=1,max do
      if not aLines[i] or not bLines[i] then
         print("[  ERROR ] Strings have diffrent number of lines:")
         flagLine = i
         break
      elseif aLines[i] ~= bLines[i] then
         print("[  ERROR ] Strings have Mismatch:")
         flagLine = i
         break
      end
   end

   for i=1,max do
      print("a: ", createPrintableLine(aLines[i]))
      print("b: ", createPrintableLine(bLines[i]))
      if i == flagLine then
         print("^^^^^^")
      end
   end

   return false
end

function checkEqual(a, b)
   local pass = stringCheck(a, b)
   assert(pass)
end

function documentWrap(text)
   local startTag = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'
   local endTag   = '</svg>'
   return string.format("%s\n%s%s\n", startTag, text, endTag)
end

local testlist = {}

function testlist.document()

   local root = SVGW.Document:new(10, 20)
   checkEqual(root:createText(2), [[
<svg width="10" height="20" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
</svg>
]])

   local root = SVGW.Document:new()
   root:setSize(300, 600)
   checkEqual(root:createText(2), [[
<svg width="300" height="600" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
</svg>
]])

end

function testlist.shapes1()

   local root = SVGW.Document:new()
   root:add(SVGW.Rect:new(5, 5, 10, 10))
   root:add(SVGW.Circle:new(20, 20, 5))
   root:add(SVGW.Ellipse:new(25, 15, 20, 55))
   root:add(SVGW.Line:new(10, 10, 20, 20))
   root:add(SVGW.Polygon:new({ {x = 5, y = 6}, {x = 7, y = 8}, {x = 8, y = 7} }))
   root:add(SVGW.Polyline:new({ {x = 3, y = 4}, {x = 5, y = 6}, {x = -4, y = -7} }))
   local path = SVGW.Path:new()
   root:add(path)
   path:addMoveToAbs(10, 10)
   path:addLineToRel(5, 5)
   path:addLineToRel(5, -5)
   path:addClosePath()
   root:add(SVGW.Text:new("Hello World!", 3, 4))

   checkEqual(root:createText(2), documentWrap([[
  <rect x="5" y="5" width="10" height="10" />
  <circle cx="20" cy="20" r="5" />
  <ellipse cx="25" cy="15" rx="20" ry="55" />
  <line x1="10" y1="10" x2="20" y2="20" />
  <polygon points="5,6 7,8 8,7" />
  <polyline points="3,4 5,6 -4,-7" />
  <path d="M10,10 l5,5 l5,-5 Z" />
  <text x="3" y="4">Hello World!</text>
]]))
end

function testlist.shapes2()

   local root = SVGW.Document:new()
   root:addRect(1, 2, 3, 4)
   root:addCircle(1, 2, 3)
   root:addEllipse(1, 2, 3, 4)
   root:addLine(1, 2, 3, 4)
   root:addPolygon({ {x = 1, y = 2}, {x = 3, y = 4}, {x = 5, y = 6} })
   root:addPolyline({ {x = 6, y = 5}, {x = 4, y = 3}, {x = -2, y = -1} })
   local path = root:addPath()
   path:addMoveToAbs(1, 2)
   path:addLineToRel(3, 4)
   path:addLineToRel(5, -6)
   path:addClosePath()
   root:addText("Hello SVG!", 1, 2)
   root:addGroup():addRect()

   checkEqual(root:createText(2), documentWrap([[
  <rect x="1" y="2" width="3" height="4" />
  <circle cx="1" cy="2" r="3" />
  <ellipse cx="1" cy="2" rx="3" ry="4" />
  <line x1="1" y1="2" x2="3" y2="4" />
  <polygon points="1,2 3,4 5,6" />
  <polyline points="6,5 4,3 -2,-1" />
  <path d="M1,2 l3,4 l5,-6 Z" />
  <text x="1" y="2">Hello SVG!</text>
  <g>
    <rect />
  </g>
]]))
end

function testlist.color()
   -- Grey Test
   checkEqual("#000000", SVGW.Color.Grey(-1))
   checkEqual("#000000", SVGW.Color.Grey(0))
   checkEqual("#ffffff", SVGW.Color.Grey(1))
   checkEqual("#ffffff", SVGW.Color.Grey(2))
   checkEqual("#808080", SVGW.Color.Grey(0.5))
   -- Check Grey Rounding
   checkEqual("#202020", SVGW.Color.Grey(0.125))
   checkEqual("#202020", SVGW.Color.Grey(0.126))
   checkEqual("#212121", SVGW.Color.Grey(0.1275))
   checkEqual("#212121", SVGW.Color.Grey(0.129))
   
   -- RBG Test
   checkEqual("#000000", SVGW.Color.RGB(-1, -1, -1))
   checkEqual("#000000", SVGW.Color.RGB(0, 0, 0))
   checkEqual("#ffffff", SVGW.Color.RGB(1, 1, 1))
   checkEqual("#ffffff", SVGW.Color.RGB(2, 2, 2))
   checkEqual("#808080", SVGW.Color.RGB(0.5, 0.5, 0.5))
   checkEqual("#0080ff", SVGW.Color.RGB(0, 0.5, 1))
   
   -- HSL Test
   -- See the hsl example for more verification
   checkEqual("#000000", SVGW.Color.HSL(-1, -1, -1))
   checkEqual("#000000", SVGW.Color.HSL(0, 0, 0))
   checkEqual("#ffffff", SVGW.Color.HSL(1, 1, 1))
   checkEqual("#ffffff", SVGW.Color.HSL(2, 2, 2))
end

function testlist.polygon()
   local root = SVGW.Document:new()
   local p = SVGW.Polygon:new()
   root:add(p)
   p:add(5, 6)
   p:add(14, 55)
   p:add(34, 67.6)
   p:add("56px", "33cm")
   root:add(SVGW.Polygon:new():add(1,2):add(3,4):add(5,6))

   checkEqual(root:createText(2), documentWrap([[
  <polygon points="5,6 14,55 34,67.6 56px,33cm" />
  <polygon points="1,2 3,4 5,6" />
]]))
end

function testlist.polyline()
   local root = SVGW.Document:new()
   local p = SVGW.Polyline:new()
   root:add(p)
   p:add(5, 6)
   p:add(14, 55.66)
   p:add(34, 67.6)
   p:add("56px", "33cm")
   root:add(SVGW.Polyline:new():add(1,2):add(3,4):add(5,6))

   checkEqual(root:createText(2), documentWrap([[
  <polyline points="5,6 14,55.66 34,67.6 56px,33cm" />
  <polyline points="1,2 3,4 5,6" />
]]))
end

function testlist.style()
  local root = SVGW.Document:new()
   root:add(SVGW.Rect:new(20, 20, "100", 100):setStyle({ fill="red", stroke="black", stroke_width=2 }))
   root:addRect(10, 10, 10, 10):setStroke("black"):setStrokeWidth(4):setFill("blue")
   root:addRect(10, 10, 10, 10):setStroke("green", 3):noFill():setOpacity(0.5)

   checkEqual(root:createText(2), documentWrap([[
  <rect x="20" y="20" width="100" height="100" style="stroke:black;stroke-width:2;fill:red;" />
  <rect x="10" y="10" width="10" height="10" style="stroke:black;stroke-width:4;fill:blue;" />
  <rect x="10" y="10" width="10" height="10" style="stroke:green;stroke-width:3;fill:none;opacity:0.5;" />
]]))

   local style = SVGW.Style:new(1, 2, 3, 4, 5)
   assert(style.styleAttributes.stroke == 1)
   assert(style.styleAttributes.stroke_width == 2)
   assert(style.styleAttributes.fill == 3)
   assert(style.styleAttributes.fill_opacity == 4)
   assert(style.styleAttributes.stroke_opacity == 5)
end

function testlist.path()
   local root = SVGW.Document:new()
   local path = SVGW.Path:new()
   root:add(path)
   path:addMoveToAbs(10, 11)
   path:addMoveToRel(15, 16)
   path:addLineToAbs(5, 7)
   path:addLineToRel(6, -5)
   path:addHorizontalLineToAbs(3)
   path:addHorizontalLineToRel(-3)
   path:addVerticalLineToAbs(7)
   path:addVerticalLineToRel(-7)
   path:addCubicCurveToAbs(1,2,3,4,5,6)
   path:addNextCubicCurveToAbs(1,2,3,4)
   path:addCubicCurveToRel(1,2,3,4,5,6)
   path:addNextCubicCurveToRel(1,2,3,4)
   path:addQuadraticCurveToAbs(1,2,3,4)
   path:addNextQuadraticCurveToAbs(1, 2)
   path:addQuadraticCurveToRel(1,2,3,4)
   path:addNextQuadraticCurveToRel(1, 2)
   path:addArcAbs(2,3,4,5,6,true,true)
   path:addArcRel(2,3,4,5,6,true,true)
   path:addClosePath()

   local path = SVGW.Path:new()
   root:add(path)
   path:addArcAbs(2,3, 6)
   path:addArcRel(2,3,4,5,6,false,false)
   path:addArcAbs(2,3,4,5,6,"0","1")
   path:addArcRel(2,3,4,5,6,"1","0")

   root:add(SVGW.Path:new():addMoveToAbs(10, 11):addMoveToRel(15, 16):addLineToAbs(5, 7):addLineToRel(6, -5):addHorizontalLineToAbs(3):addHorizontalLineToRel(-3):addVerticalLineToAbs(7):addVerticalLineToRel(-7):addCubicCurveToAbs(1,2,3,4,5,6):addNextCubicCurveToAbs(1,2,3,4):addCubicCurveToRel(1,2,3,4,5,6):addNextCubicCurveToRel(1,2,3,4):addQuadraticCurveToAbs(1,2,3,4):addNextQuadraticCurveToAbs(1, 2):addQuadraticCurveToRel(1,2,3,4):addNextQuadraticCurveToRel(1, 2):addArcAbs(2,3,4,5,6,true,true):addArcRel(2,3,4,5,6,true,true):addClosePath():addClosePath())


   checkEqual(root:createText(2), documentWrap([[
  <path d="M10,11 m15,16 L5,7 l6,-5 H3 h-3 V7 v-7 C3,4 5,6 1,2 S3,4 1,2 c3,4 5,6 1,2 s3,4 1,2 Q3,4 1,2 T1,2 q3,4 1,2 t1,2 A4,5 6 1,1 2,3 a4,5 6 1,1 2,3 Z" />
  <path d="A6,6 0 0,0 2,3 a4,5 6 0,0 2,3 A4,5 6 0,1 2,3 a4,5 6 1,0 2,3" />
  <path d="M10,11 m15,16 L5,7 l6,-5 H3 h-3 V7 v-7 C3,4 5,6 1,2 S3,4 1,2 c3,4 5,6 1,2 s3,4 1,2 Q3,4 1,2 T1,2 q3,4 1,2 t1,2 A4,5 6 1,1 2,3 a4,5 6 1,1 2,3 Z Z" />
]]))
end

function testlist.transform()
  local root = SVGW.Document:new()
  
  root:add(SVGW.Rect:new(10, 10, 10, 10):matrix(1,2,3,4,5,6):translate(1,2):scale(1,2):rotate(1,2,3):skewX(1):skewY(2))
  
  checkEqual(root:createText(2), documentWrap([[
  <rect transform="matrix(1,2,3,4,5,6) translate(1,2) scale(1,2) rotate(1,2,3) skewX(1) skewY(2)" x="10" y="10" width="10" height="10" />
]]))
end

function testlist.use()
   local root = SVGW.Document:new()
   local c = SVGW.Circle:new(0, 0, 10)
   root:addDef(c, "circle-test")
   root:add(SVGW.Use:new(c, 10, 11))
   checkEqual(root:createText(2), documentWrap([[
  <defs>
    <circle id="circle-test" cx="0" cy="0" r="10" />
  </defs>
  <use xlink:href="#circle-test" x="10" y="11" />
]]))
  
  
end

function testlist.linearGradent()
   local root = SVGW.Document:new()
  
   local defaultGradient = SVGW.LinearGradient:new()
   root:addDef(defaultGradient, "defaultGradient")

   local positionGradient = SVGW.LinearGradient:new(nil, 1, 2, 3, 4)
   root:addDef(positionGradient, "positionGradient")

   local oneColorGradient = SVGW.LinearGradient:new("red", 4, 3, 2, 1)
   root:addDef(oneColorGradient, "oneColorGradient")

   local threeColorGradient = SVGW.LinearGradient:new({
      { offset="0%",   color="red",   opacity=1   },
      { offset="50%",  color="green", opacity=0.5 },
      { offset="100%", color="blue",  opacity=1   }
   })
   root:addDef(threeColorGradient, "threeColorGradient")
   
   local threeColorGradientNoOpacity = SVGW.LinearGradient:new({
      { offset="0%",   color="red"   },
      { offset="50%",  color="green" },
      { offset="100%", color="blue"  }
   })
   root:addDef(threeColorGradientNoOpacity, "threeColorGradientNoOpacity")

   local rectStyle = SVGW.Style:new("black", 2, defaultGradient )
   root:add(SVGW.Rect:new(10, 10, 10, 10):setStyle(rectStyle))
   
   rectStyle:setFill(positionGradient)

   root:add(SVGW.Rect:new(20, 20, 20, 20):setStyle(rectStyle))

   checkEqual(root:createText(2), documentWrap([[
  <defs>
    <linearGradient id="defaultGradient">
      <stop offset="0%" style="stop-color:black;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:white;stop-opacity:0;" />
    </linearGradient>
    <linearGradient id="positionGradient" x1="1" y1="2" x2="3" y2="4">
      <stop offset="0%" style="stop-color:black;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:white;stop-opacity:0;" />
    </linearGradient>
    <linearGradient id="oneColorGradient" x1="4" y1="3" x2="2" y2="1">
      <stop offset="0%" style="stop-color:red;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:red;stop-opacity:0;" />
    </linearGradient>
    <linearGradient id="threeColorGradient">
      <stop offset="0%" style="stop-color:red;stop-opacity:1;" />
      <stop offset="50%" style="stop-color:green;stop-opacity:0.5;" />
      <stop offset="100%" style="stop-color:blue;stop-opacity:1;" />
    </linearGradient>
    <linearGradient id="threeColorGradientNoOpacity">
      <stop offset="0%" style="stop-color:red;stop-opacity:1;" />
      <stop offset="50%" style="stop-color:green;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:blue;stop-opacity:1;" />
    </linearGradient>
  </defs>
  <rect x="10" y="10" width="10" height="10" style="stroke:black;stroke-width:2;fill:url(#defaultGradient);" />
  <rect x="20" y="20" width="20" height="20" style="stroke:black;stroke-width:2;fill:url(#positionGradient);" />
]]))
end

function testlist.radialGradent()
   local root = SVGW.Document:new()
  
   local defaultGradient = SVGW.RadialGradient:new()
   root:addDef(defaultGradient, "defaultGradient")

   local positionGradient = SVGW.RadialGradient:new(nil, 1, 2, 3, 4, 5)
   root:addDef(positionGradient, "positionGradient")

   local oneColorGradient = SVGW.RadialGradient:new("red", 5, 4, 3, 2, 1)
   root:addDef(oneColorGradient, "oneColorGradient")

   local threeColorGradient = SVGW.RadialGradient:new({
      { offset="0%",   color="red",   opacity=1   },
      { offset="50%",  color="green", opacity=0.5 },
      { offset="100%", color="blue",  opacity=1   }
   })
   root:addDef(threeColorGradient, "threeColorGradient")
   
   local threeColorGradientNoOpacity = SVGW.RadialGradient:new({
      { offset="0%",   color="red"   },
      { offset="50%",  color="green" },
      { offset="100%", color="blue"  }
   })
   root:addDef(threeColorGradientNoOpacity, "threeColorGradientNoOpacity")

   local rectStyle = SVGW.Style:new("black", 2, defaultGradient )
   root:add(SVGW.Rect:new(10, 10, 10, 10):setStyle(rectStyle))
   
   rectStyle:setFill(positionGradient)

   root:add(SVGW.Rect:new(20, 20, 20, 20):setStyle(rectStyle))

   checkEqual(root:createText(2), documentWrap([[
  <defs>
    <radialGradient id="defaultGradient">
      <stop offset="0%" style="stop-color:black;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:white;stop-opacity:0;" />
    </radialGradient>
    <radialGradient id="positionGradient" cx="1" cy="2" r="3" fx="4" fy="5">
      <stop offset="0%" style="stop-color:black;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:white;stop-opacity:0;" />
    </radialGradient>
    <radialGradient id="oneColorGradient" cx="5" cy="4" r="3" fx="2" fy="1">
      <stop offset="0%" style="stop-color:red;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:red;stop-opacity:0;" />
    </radialGradient>
    <radialGradient id="threeColorGradient">
      <stop offset="0%" style="stop-color:red;stop-opacity:1;" />
      <stop offset="50%" style="stop-color:green;stop-opacity:0.5;" />
      <stop offset="100%" style="stop-color:blue;stop-opacity:1;" />
    </radialGradient>
    <radialGradient id="threeColorGradientNoOpacity">
      <stop offset="0%" style="stop-color:red;stop-opacity:1;" />
      <stop offset="50%" style="stop-color:green;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:blue;stop-opacity:1;" />
    </radialGradient>
  </defs>
  <rect x="10" y="10" width="10" height="10" style="stroke:black;stroke-width:2;fill:url(#defaultGradient);" />
  <rect x="20" y="20" width="20" height="20" style="stroke:black;stroke-width:2;fill:url(#positionGradient);" />
]]))
end

function testlist.marker()
   local root = SVGW.Document:new()

   local markerStyle = SVGW.Style:new("black", 2, "white") 
   local markerCap = SVGW.Marker:new(1, 2, 3, 4, 5, 6):setStyle(markerStyle)
   local markerMid = SVGW.Marker:new(6, 5, 4, 3, 2, 1):setStyle(markerStyle)

   markerCap:add(SVGW.Rect:new(10, 10, 10, 10))
   markerMid:add(SVGW.Circle:new(10, 10, 5))

   root:addDef(markerCap, "markerCap")
   root:addDef(markerMid, "markerMid")

   local pathStyle = SVGW.Style:new("black", 2, "none"):add({ 
      marker_start=markerCap,
      marker_mid=markerMid,
      marker_end=markerCap
   })

   root:add(SVGW.Path:new():setStyle(pathStyle):addMoveToAbs(0,0):addLineToAbs(10,10):addLineToAbs(20, 0))

   checkEqual(root:createText(2), documentWrap([[
  <defs>
    <marker id="markerCap" markerWidth="1" markerHeight="2" refX="3" refY="4" orient="5" markerUnits="6" style="stroke:black;stroke-width:2;fill:white;">
      <rect x="10" y="10" width="10" height="10" />
    </marker>
    <marker id="markerMid" markerWidth="6" markerHeight="5" refX="4" refY="3" orient="2" markerUnits="1" style="stroke:black;stroke-width:2;fill:white;">
      <circle cx="10" cy="10" r="5" />
    </marker>
  </defs>
  <path d="M0,0 L10,10 L20,0" style="stroke:black;stroke-width:2;fill:none;marker-start:url(#markerCap);marker-mid:url(#markerMid);marker-end:url(#markerCap);" />
]]))
end

function testlist.pattern()
   local root = SVGW.Document:new()

   local pattern = SVGW.Pattern:new(1, 2, 3, 4, 5, 6):setID("pattern")

   pattern:addRect(2, 2, 6, 6)

   local style = SVGW.Style:new("black", 2, pattern)

   root:addRect(0, 0, 50, 50):setStyle(style)

   checkEqual(root:createText(2), documentWrap([[
  <defs>
    <pattern id="pattern" x="1" y="2" width="3" height="4" patternUnits="5" patternContentUnits="6">
      <rect x="2" y="2" width="6" height="6" />
    </pattern>
  </defs>
  <rect x="0" y="0" width="50" height="50" style="stroke:black;stroke-width:2;fill:url(#pattern);" />
]]))
end

function testlist.documentBackground()
   local style = SVGW.Style:new("black", "5", "white")
   local root = SVGW.Document:new(100, 100, style)

   
   checkEqual(root:createText(2), [[
<svg width="100" height="100" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <rect x="0" y="0" width="100" height="100" style="stroke:none;fill:white;" />
</svg>
]])
end

function testlist.text()
   local root = SVGW.Document:new()
  
   local style = SVGW.Style:new():setOnlyFill("black")

   root:add(SVGW.Text:new("Center Test", 5, 6):setStyle(style):setAnchorPointMiddle())

   
   checkEqual(root:createText(2), documentWrap([[
  <text x="5" y="6" style="stroke:none;fill:black;text-anchor:middle;dominant-baseline:middle;">Center Test</text>
]]))
end

function testlist.refrenceDetection()
   local root = SVGW.Document:new()
   local group1 = SVGW.Group:new():setID("group1ID")
   local group2 = SVGW.Group:new():setID("group2ID")
   local group3 = SVGW.Group:new():setID("group3ID")
    -- Intentionaly Delcared but not used
   local group4 = SVGW.Group:new():setID("group4ID")
   
   group1:add(SVGW.Use:new(group2))
   root:add(SVGW.Use:new(group1))
   
   
   root:add(SVGW.Use:new(group3))    
   root:add(group3)
    
   checkEqual(root:createText(2), documentWrap([[
  <defs>
    <g id="group1ID">
      <use xlink:href="#group2ID" />
    </g>
    <g id="group2ID">
    </g>
  </defs>
  <use xlink:href="#group1ID" />
  <use xlink:href="#group3ID" />
  <g id="group3ID">
  </g>
]]))
    
end

function testlist.autoID()
   local root = SVGW.Document:new()
   local group1 = SVGW.Group:new()
   local group2 = SVGW.Group:new()
   local group3 = SVGW.Group:new()

   
   group1:add(SVGW.Use:new(group2))
   root:add(SVGW.Use:new(group1))
   
   
   root:add(SVGW.Use:new(group3))    
   root:add(group3)
    
   checkEqual(root:createText(2), documentWrap([[
  <defs>
    <g id="unique-id-0001">
      <use xlink:href="#unique-id-0002" />
    </g>
    <g id="unique-id-0002">
    </g>
  </defs>
  <use xlink:href="#unique-id-0001" />
  <use xlink:href="#unique-id-0003" />
  <g id="unique-id-0003">
  </g>
]]))
    
end


--- Run all the tests
for k, v in pairs(testlist) do
   print(string.format("[  Test  ] %s", k))
   v()
   print("[  PASS  ]")
end
print("[  PASS  ] All Tests Pass")


-- Run Tests
print("[ ==== Running Tests === ]")
print()
dofile("test/test.lua")
print()

local examples = {
   "redBox.lua",
   "hsl.lua",
   "use.lua",
   "gradient.lua",
   "marker.lua",
   "hueKey.lua",
   "shapes.lua",
   "pattern.lua"
}

-- Run Examples
print("[ == Running Examples == ]")
print()
for i, filename in ipairs(examples) do
   print("Running: " .. filename)
   dofile("examples/" .. filename)
end
print()
print("[ ======== Done ======== ]")


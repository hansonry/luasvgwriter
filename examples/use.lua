local SVGW = require "SVGWriter"

local doc = SVGW.Document:new(100, 100)
local style = SVGW.Style:new("black", 1, "red")

local box = SVGW.Rect:new(0, 0, 10, 10, 1, 1)
-- You can name the element if you want, otherwise it will be automaticaly
-- named
box:setID("round-rect")  

local fg = SVGW.Group:new()
doc:addDef(fg)

fg:add(SVGW.Use:new(box, 10, 10):setStyle(style):rotate(45, 15, 15))
style:setFill("blue")
fg:add(SVGW.Use:new(box, 30, 10):setStyle(style))
style:setFill("green")
fg:add(SVGW.Use:new(box, 10, 30):setStyle(style))
style:setFill("yellow")
fg:add(SVGW.Use:new(box, 30, 30):setStyle(style):rotate(45, 35, 35))

doc:add(SVGW.Use:new(fg, 0, 0))

doc:add(SVGW.Use:new(fg, 50, 0):rotate(90, 75, 25))

doc:add(SVGW.Use:new(fg, 0, 50):rotate(270, 25, 75))

doc:add(SVGW.Use:new(fg, 50, 50):rotate(180, 75, 75))

doc:writeToFile("examples/output/use.svg")

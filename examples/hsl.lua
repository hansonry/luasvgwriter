local SVGW = require "SVGWriter"

local doc = SVGW.Document:new(100, 210)

for i=0,9 do
   local color = SVGW.Color.HSL(i / 10.0, 0.5, 0.5)
   doc:add(SVGW.Rect:new(i * 10, 0, 10, 10):setOnlyFill(color))
   for k=0,9 do
      local color = SVGW.Color.HSL(i / 10.0, k / 10.0, 0.5)
      doc:add(SVGW.Rect:new(i * 10, k * 10 + 10, 10, 10):setOnlyFill(color))
   end
   for k=0,9 do
      local color = SVGW.Color.HSL(i / 10.0, 0.5, k / 10.0)
      doc:add(SVGW.Rect:new(i * 10, k * 10 + 110, 10, 10):setOnlyFill(color))
   end
end

doc:writeToFile("examples/output/hsl.svg")

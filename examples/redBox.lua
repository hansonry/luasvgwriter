--- redBox.lua

-- Get a handle to the library
local SVGW = require "SVGWriter"

-- Create a new SVG document
--                           width
--                           |   height
local doc = SVGW.Document:new(30, 30)

-- Creates a rectangle and adds it to the documentation. Also rotates
-- the rectangle 45 degrees on center
--          x
--          |   y                    stroke                    angle(degrees)
--          |   |   width            |        stroke-width     |   around x
--          |   |   |   height       |        |  fill          |   |   around y
doc:addRect(10, 10, 10, 10):setStyle("black", 2, "red"):rotate(45, 15, 15)

-- Write document to file
doc:writeToFile("examples/output/redBox.svg")

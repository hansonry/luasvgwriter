local SVGW = require "SVGWriter"

local doc = SVGW.Document:new(100, 200)

local markerStyle = SVGW.Style:new("black", 1, "red")

local diamondMarker = SVGW.Marker:new(10, 10, 5, 5):setStyle(markerStyle)
diamondMarker:add(SVGW.Rect:new(2, 2, 6, 6):rotate(45, 5, 5))

local circleMarker = SVGW.Marker:new(10, 10, 5, 5):setStyle(markerStyle)
circleMarker:add(SVGW.Circle:new(5, 5, 4))

local arrowMarker = SVGW.Marker:new(10, 10, 5, 5, "auto-start-reverse"):setStyle(markerStyle)
arrowMarker:add(SVGW.Path:new():addMoveToAbs(1,1):addLineToAbs(9,5):addLineToAbs(1,9):addClosePath())

local pathStyle = SVGW.Style:new("black", 2):noFill():add({
   marker_start = diamondMarker,
   marker_mid   = circleMarker,
   marker_end   = diamondMarker
})

doc:add(SVGW.Path:new():addMoveToAbs(20, 20):addLineToAbs(50, 80):addLineToAbs(80, 20):setStyle(pathStyle))

pathStyle:add({
   marker_start = arrowMarker,
   marker_end   = arrowMarker
})

doc:add(SVGW.Path:new():addMoveToAbs(20, 120):addLineToAbs(50, 180):addLineToAbs(80, 120):setStyle(pathStyle))


-- Write document to file
doc:writeToFile("examples/output/marker.svg")


-- Get a handle to the library
local SVGW = require "SVGWriter"

-- Create a new SVG document

local doc = SVGW.Document:new(180, 140)


local patternStyle = SVGW.Style:new("black", 2, "#80ee80"):add({stroke_linecap="round"})

local pattern = SVGW.Pattern:new(0, 0, 10, 10, "userSpaceOnUse")

-- Really Big Background Rect for the color
pattern:addRect(-10, -10, 30, 30):setStyle(patternStyle)
-- Make a cross
pattern:addLine(3, 3, 7, 7):setStyle(patternStyle)
pattern:addLine(3, 7, 7, 3):setStyle(patternStyle)

local style = SVGW.Style:new("black", 2, pattern)

doc:addRect(10, 10, 50, 50):setStyle(style)
doc:addRect(70, 10, 100, 50):setStyle(style)
doc:addEllipse(90, 100, 75, 25):setStyle(style)

-- Write document to file
doc:writeToFile("examples/output/pattern.svg")


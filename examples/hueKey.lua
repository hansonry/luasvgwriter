-- Get a handle to the library
local SVGW = require "SVGWriter"

-- Setting up some constants
local saturation = 0.5
local level      = 0.5

local hueStep = 20

local sampleWidth = 60

local colorWidth  = 40
local colorHeight = 40

local paddingX = 10


local styleColor = SVGW.Style:new("black", 2)

local styleText = SVGW.Style:new():setOnlyFill("black"):setTextAnchorPointMiddle()

local styleBackground = SVGW.Style:new():setFill("white")

local doc = SVGW.Document:new(sampleWidth * hueStep + 2 * paddingX, 
                             100, styleBackground)

for i=1,hueStep do
   local hueValue = i / hueStep
   local x = (i - 1) * sampleWidth + paddingX

   -- Write the Label
   doc:add(SVGW.Text:new(string.format("%.2f", hueValue), 
                         x + colorWidth / 2, 
                         15):setStyle(styleText))
   
   -- Set the Color
   styleColor:setFill(SVGW.Color.HSL(hueValue, saturation, level))

   -- Draw The Color
   doc:add(SVGW.Rect:new(x, 30, colorWidth, colorHeight):setStyle(styleColor))
end

-- Write document to file
doc:writeToFile("examples/output/hueKey.svg")


-- Get a handle to the library
local SVGW = require "SVGWriter"

-- Create a new SVG document
local doc = SVGW.Document:new(150, 100)

local s = {x = 10, y = 10, hue = 0}

function s:nextCol()
   s.x = s.x + 20
   s.hue = s.hue + 0.05
end

function s:nextRow()
   s.y = s.y + 20
   s.x = 10
   s.hue = s.hue + 0.05
end


local style = SVGW.Style:new("black", 2)

style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addRect(s.x, s.y, 10, 10):setStyle(style)
s:nextCol()

style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addRect(s.x, s.y, 30, 10):setStyle(style)
s:nextCol()
s:nextCol()

style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addRect(s.x, s.y, 10, 20, 3, 3):setStyle(style)
s:nextCol()


style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addCircle(s.x + 5, s.y + 5, 5):setStyle(style)
s:nextCol()
s:nextCol()

style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addEllipse(s.x, s.y + 5, 15, 5):setStyle(style)
s:nextRow()
s:nextRow()

style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addLine(s.x, s.y, s.x + 10, s.y + 10):setStyle(style)
s:nextCol()

style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addPolygon():setStyle(style):add(s.x, s.y + 10):add(s.x + 5, s.y):add(s.x + 10, s.y+ 10)
s:nextCol()

style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addPolyline():setStyle(style):add(s.x, s.y + 10):add(s.x + 5, s.y):add(s.x + 10, s.y+ 10)
s:nextCol()

style:setFill(SVGW.Color.HSL(s.hue, 0.5, 0.5))
doc:addPath():setStyle(style):addMoveToAbs(s.x, s.y):addCubicCurveToRel(10, 10, 10, 0, 0, 10):addNextCubicCurveToRel(10, -10, 0, -10):addNextCubicCurveToRel(10, 10, 5, 5)
s:nextRow()


doc:addText("Test Text", s.x, s.y + 10):setOnlyFill("black")

-- Write document to file
doc:writeToFile("examples/output/shapes.svg")


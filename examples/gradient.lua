local SVGW = require "SVGWriter"

local doc = SVGW.Document:new(100, 100)

-- Setup the style
local style = SVGW.Style:new("black", 2)

--- Linear Gradient


-- Draw a background rect so we can see the transparent gradient
doc:add(SVGW.Rect:new(0, 0, 50, 25):setOnlyFill("red"))

-- Green To Transparent Gradient
local greenToTransparentGradient =  SVGW.LinearGradient:new("green")

-- Set the fill to the gradient and draw the rect
style:setFill(greenToTransparentGradient)
doc:add(SVGW.Rect:new(10, 10, 30, 30):setStyle(style))


-- Red Gree Blue Gradient
local rgbGradient =  SVGW.LinearGradient:new({
   { offset="0%",   color="red"   }, 
   { offset="50%",  color="green" },
   { offset="100%", color="blue"  } 
})

-- Set the fill to the gradient and draw the rect
style:setFill(rgbGradient)
doc:add(SVGW.Rect:new(60, 10, 30, 30):setStyle(style))


--- Radial Gradient

-- Draw a background rect so we can see the transparent gradient
doc:add(SVGW.Rect:new(0, 50, 50, 25):setOnlyFill("red"))

-- Green To Transparent Gradient
local greenToTransparentGradient =  SVGW.RadialGradient:new("green")

-- Set the fill to the gradient and draw the rect
style:setFill(greenToTransparentGradient)
doc:add(SVGW.Circle:new(25, 75, 15):setStyle(style))


-- Red Gree Blue Gradient
local rgbGradient =  SVGW.RadialGradient:new({
   { offset="0%",   color="red"   }, 
   { offset="50%",  color="green" },
   { offset="100%", color="blue"  } 
})

-- Set the fill to the gradient and draw the rect
style:setFill(rgbGradient)
doc:add(SVGW.Circle:new(75, 75, 15):setStyle(style))

--- Write out the file
doc:writeToFile("examples/output/gradient.svg")


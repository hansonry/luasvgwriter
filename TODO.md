# TODO

This is a list of work that needs to be done yet. I try to keep it in order
of what is most intresting to me.

* Text on path
* Blur
* Better Style Handling (More Functions)
* Other Effects like Blur?
* Raster Images?

Large Features I am thinking about:

* Shape to Path
* Boolean Path Operations
* Complex non-native Shapes (Like Inkscape Stars and Spirals)

